package main

import (
    "log"
	"net/http"
	"io/ioutil"
	"encoding/json"
	"math/rand"
	"time"
	"strconv"
	"os"
	"strings"
	"sync"
	"tictactoe/common"
)

type Games struct {
	G map[int]*common.Game
	sync.Mutex
}

var games *Games

func gameHandler(w http.ResponseWriter, r *http.Request) {
	gameIdStr := strings.Split(r.URL.Path, "/")[2]
	gameId, _ := strconv.Atoi(gameIdStr)
	switch r.Method {
	case "GET":
		log.Println("GET game " + r.URL.Path + ":" + gameIdStr)
		js, _ := json.Marshal(games.G[gameId])
		log.Println("Return Game: ", string(js))
		w.Header().Set("Content-Type", "application/json")
		w.Write(js)

	case "POST":
		log.Println("POST game")
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			log.Printf("Error reading body: %v", err)
			return
		}

		var playerGame common.Game;
		json.Unmarshal(body, &playerGame)
		game := games.G[gameId]
		log.Println("Player is " + playerGame.Player)
		if game.Player == playerGame.Player {
			game.Board = playerGame.Board

			if isDraw(game) {
				game.State = common.GAME_DONE
				game.Winner = "-"
			} else if isWon(game, playerGame.Player) {
				game.State = common.GAME_DONE
				game.Winner = playerGame.Player
			} else {
				if game.Player == common.PLAYER_X {
					game.Player = common.PLAYER_O
				} else {
					game.Player = common.PLAYER_X
				}
			}
		}

		js, _ := json.Marshal(game)
		w.Header().Set("Content-Type", "application/json")
		w.Write(js)
	default:
		w.Write([]byte("Unsupported"))
	}
}

func isWon(game *common.Game, player string) bool {
	log.Println("Checking win for " + player)
	log.Println(game.Board)
	won := ((game.Board[0] == player && game.Board[1] == player && game.Board[2] == player) ||
			(game.Board[3] == player && game.Board[4] == player && game.Board[5] == player) ||
			(game.Board[6] == player && game.Board[7] == player && game.Board[8] == player) ||
			(game.Board[0] == player && game.Board[3] == player && game.Board[6] == player) ||
			(game.Board[1] == player && game.Board[4] == player && game.Board[7] == player) ||
			(game.Board[2] == player && game.Board[5] == player && game.Board[8] == player) ||
			(game.Board[0] == player && game.Board[4] == player && game.Board[8] == player) ||
			(game.Board[2] == player && game.Board[4] == player && game.Board[6] == player))
	log.Println("Checking win for " + player + " : " + strconv.FormatBool(won))
	return won
}

func isDraw(game *common.Game) bool {
	log.Println("Checking draw")
	draw := (game.Board[0] != " " && game.Board[1] != " " && game.Board[2] != " " &&
		game.Board[3] != " " && game.Board[4] != " " && game.Board[5] != " " &&
		game.Board[6] != " " && game.Board[7] != " " && game.Board[8] != " ")
	log.Println("Checking draw: " + strconv.FormatBool(draw))
	return draw
}

func startHandler(w http.ResponseWriter, r *http.Request) {
	log.Println("START game")
	Games.Lock()
	// Iterate the games and check for an available game
	var g *common.Game;
	for _, game := range games.G {
		if game.State == common.GAME_NEW {
			// This game is waiting for another player.
			// Set this player and start the game.
			game.State = common.GAME_ACTIVE
			g = game
			break;
		}
	}

	if g == nil {
		// Create a new game with a random id and the player as X
		gameId := rand.Intn(20000-1000) + 1000
		game := common.Game{Id: gameId, Player: common.PLAYER_X, State:common.GAME_NEW, Board:[9]string{" "," "," "," "," "," "," "," "," "}}
		games.G[gameId] = &game
		g = &game
	}
	Games.Unlock()
	js, _ := json.Marshal(*g)
	log.Println("Game started: ", g.Id)
	w.Header().Set("Content-Type", "application/json")
  	w.Write(js)
}

func main() {
	log.SetOutput(os.Stdout)
	rand.Seed(time.Now().UnixNano())
	games = &Games{G : make(map[int]*common.Game)}
	http.HandleFunc("/tictactoe/", gameHandler)
	http.HandleFunc("/tictactoe/start", startHandler)
    log.Fatal(http.ListenAndServe(":8080", nil))
}

