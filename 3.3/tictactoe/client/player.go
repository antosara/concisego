package main

import (
	"log"
	"os"
	"fmt"
	"bufio"	
	"strings"
	"net/http"
	"encoding/json"
	"time"
	"io/ioutil"
	"strconv"
	"bytes"
	"tictactoe/common"
)


const BASE_URL = "http://localhost:8080/tictactoe"
const START_URL = BASE_URL + "/start"


var game common.Game

func readTrimmed(reader *bufio.Reader) (string, error) {
	str, err := reader.ReadString('\n')
	return strings.Replace(strings.TrimSpace(str)," ", "_", -1), err
}

func printBoard() {
	fmt.Println("-------------")
	fmt.Println("| " + game.Board[0] + " | " + game.Board[1] + " | " + game.Board[2] + " |" )
	fmt.Println("-------------")
	fmt.Println("| " + game.Board[3] + " | " + game.Board[4] + " | " + game.Board[5] + " |" )
	fmt.Println("-------------")
	fmt.Println("| " + game.Board[6] + " | " + game.Board[7] + " | " + game.Board[8] + " |" )
	fmt.Println("-------------")
}

func main() {
	// Setup a reader from standard input
	reader := bufio.NewReader(os.Stdin)

	// Intro to excite the player :-)
	fmt.Println("+++++++++++++++++++++++++++++++++++++")
	fmt.Println("Welcome to TicTacToe!! ")
	fmt.Println("+++++++++++++++++++++++++++++++++++++")

	for {
		// Initialize to keep track of the player to blank
		// Once the start request is sent, the player is set
		// and remains the same for the rest of the game
		player := ""

		// Info for the user
		fmt.Println("Starting a new game. Please wait...")

		// Call the START_URL endpoint to start/join a game
		resp, err := http.Get(START_URL)
		if err != nil {
			log.Fatalln(err)
		}

		// Read the response from game server
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Fatalln(err)
		}

		// Deserialize response to the "game" data structure
		json.Unmarshal(body, &game)

		// Make sure we close the response body
		resp.Body.Close()

		// If the server responded with game state of GAME_NEW
		// this is the player that started a game. 
		// The starting player will be assigned "X" or "O" on server
		// which we get here too.
		// If server responded with GAME_ACTIVE, this player
		// joined a game. This player should be assigned the other mark		
		if game.State == common.GAME_NEW {
			player = game.Player
		} else {
			if game.Player == common.PLAYER_X {
				player = common.PLAYER_O
			} else {
				player = common.PLAYER_X
			}
		}

		// If we have received GAME_NEW, we just
		// wait for another player to join and start the game
		// Keep fetching the game every second, until that happens
		for game.State == common.GAME_NEW {
			// Get the game using the game endpoint.
			// We know the game Id by now
			resp, err := http.Get(BASE_URL + "/" + strconv.Itoa(game.Id))
			if err != nil {
				log.Fatalln(err)
			}
			body, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				log.Fatalln(err)
			}
			json.Unmarshal(body, &game)
			resp.Body.Close()
			time.Sleep(1 * time.Second)
		}

		// If we are here, the server has two players and the game is started.
		fmt.Println("***********************")
		fmt.Println("Game " + strconv.Itoa(game.Id) + " started. You are: " + player)
		fmt.Println("***********************")

		// We start a forever loop until the game is done.
		// There are certain things like showing a message once
		// until some change occurs. We use the marked flag for that.
		var marked = true
		for game.State != common.GAME_DONE {
			// Pause for a second before we send the next game update request
			time.Sleep(1 * time.Second)
			resp, _ := http.Get(BASE_URL + "/" + strconv.Itoa(game.Id))
			body, _ := ioutil.ReadAll(resp.Body)

			// Read the game
			json.Unmarshal(body, &game)

			// If the game is done, exit the loop
			if game.State == common.GAME_DONE {
				break
			}

			// If this player is NOT the current game player,
			// we print a wait message and go back to the start
			// of the game loop
			if game.Player != player {
				// We need to show this message only once until
				// some state changes
				if marked {
					printBoard()
					fmt.Println("Waiting for other player...")
					marked = false
				}
				// Give extra time
				time.Sleep(1 * time.Second)				
				continue
			} else {
				// It is this player's turn. Print any board update
				printBoard()
				
				// Track the player choice. The squares are numbered
				// 0 to 8 sequentially right and down.
				choice := -1

				// Ask for a valid choice. Any square between 0 and 8
				// inclusive and containing " " as value is acceptable
				// Keep asking until we get it
				for choice == -1 {
					fmt.Print("Fill square (0-8): ")
					c, _ := readTrimmed(reader)
					choice, _ = strconv.Atoi(c)

					if choice < 0 || choice > 8 || game.Board[choice] != " " {
						fmt.Println("Invalid choice.")
						choice = -1
					}
				}

				// Update the player's board
				game.Board[choice] = player

				// Reset the state for any messages
				marked = true

				// Post the game data as JSON to the game endpoint
				postBody, _ := json.Marshal(game)
				postResp, _ := http.Post(BASE_URL + "/" +  strconv.Itoa(game.Id), "application/json", bytes.NewBuffer(postBody))
				postResp.Body.Close()
			}
		}

		// We will be here when one of these happen
		// -> Game is a tie
		// -> Game has winner
		// We can find it from the game.Winner field

		// Give appropriate message to the player
		printBoard()
		fmt.Println("***********************")
		if game.Winner == "-" {
			fmt.Println("It's a tie, try harder next time!!")
		} else if player == game.Winner {
			fmt.Println("You won, keep it up!!")
		} else {
			fmt.Println("You lost, but good luck next time!!")
		}
		fmt.Println("***********************")

		// Let the player choose to play another game or exit
		fmt.Println("Enter 'y' if you want to play again: ")
		d, _ := readTrimmed(reader)
		if d != "y" {
			fmt.Println("Thanks for playing!!")
			break;
		}
	}
}

