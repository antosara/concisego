package common


const GAME_NEW = 0
const GAME_ACTIVE = 1
const GAME_DONE = 2

const PLAYER_X = "X"
const PLAYER_O = "O"

type Game struct {
	Id int
	State int
	Player string
	Board [9]string
	Winner string
} 
