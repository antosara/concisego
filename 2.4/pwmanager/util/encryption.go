package util

import (
	"crypto/rand"
	"golang.org/x/crypto/scrypt"
	"crypto/aes"
	"crypto/cipher"
	"encoding/base64"
	"strings"
)

func BuildKey(password []byte, salt []byte) ([]byte, []byte, error) {
	if salt == nil {
		salt = make([]byte, 16)
		_, err := rand.Read(salt)
		if err != nil {
			return nil, salt, err
		}
	}	
	uKey, err := scrypt.Key(password, salt, 32768, 8, 1, 32)
	return uKey, salt, err
}


func Encrypt(plaintext []byte, key [] byte) ([]byte, []byte, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, nil, err
	}

	aesgcm, err := cipher.NewGCM(block)
	if err != nil {
		return nil, nil, err
	}

	nonce := make([]byte, 12)
	_, err = rand.Read(nonce)
	if err != nil {
		return nil, nil, err
	}

	ciphertext := aesgcm.Seal(nil, nonce, plaintext, nil)

	return ciphertext, nonce, nil
}

func Decrypt(ciphertext []byte, key []byte, nonce []byte) ([]byte, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}

	aesgcm, err := cipher.NewGCM(block)
	if err != nil {
		return nil, err
	}

	plaintext, err := aesgcm.Open(nil, nonce, ciphertext, nil)

	return plaintext, nil
}

func EncryptToString(key []byte, str string) string {
	enc, nonce, _ := Encrypt([]byte(str), key)
	return base64.StdEncoding.EncodeToString(nonce) +
		"#" + base64.StdEncoding.EncodeToString(enc)
}

func DecryptString(key []byte, enc string) (plainText string) {
	splits := strings.Split(enc, "#")
	nonce, _ := base64.StdEncoding.DecodeString(splits[0])
	cipherText, _ := base64.StdEncoding.DecodeString(splits[1])
	plainTextBytes, _ := Decrypt(cipherText, key, nonce)
	plainText = string(plainTextBytes)
	return
}