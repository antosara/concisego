package util

import (
	"os"
	"testing"
)

func TestWrite(t *testing.T) {
	os.Remove("test_db.csv")
	newAcc := Account {"acc","user","pw"}
	db := CSVDatabase {[]Account {}}
	err := db.Write("test_db.csv", &newAcc)
	if err != nil {
		t.Errorf("Expected write did not happen")
		t.Log(err)
	}

	err = db.Read("test_db.csv")

	if err != nil {
		t.Errorf("Expected read did not happen")
		t.Log(err)
	}

	acc_len := len(db.Accounts)
	if acc_len != 1 {
		t.Errorf("Expected accounts length: 1, Got %d", acc_len)
	}
	os.Remove("test_db.csv")
}

func TestRead(t *testing.T) {
	var dbAccessor DatabaseAccessor = &CSVDatabase {[]Account {}}
	err := dbAccessor.Read("test_db1.csv")

	if(err == nil) {
		t.Errorf("Expected error not thrown")
	}	
}
