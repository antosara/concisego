package main

import (
	"os"
	"fmt"
	"bufio"	
	"pwmanager/util"
	"strings"
	"bytes"
	"syscall"
	"encoding/base64"
	"golang.org/x/crypto/ssh/terminal"
	"io/ioutil"
)

const DefaultDbFile = "my_pwds.csv"

func readTrimmed(reader *bufio.Reader) (string, error) {
	str, err := reader.ReadString('\n')
	return strings.TrimSpace(str), err
}

func promptFor(prompt string, reader *bufio.Reader) (val string, err error) {
	fmt.Print(" ", prompt, ": ")
	val, err = readTrimmed(reader)
	if err != nil {
		return "", nil
	}
	return
}

func readPassword(confirm bool) []byte {
	fmt.Print("Enter Password: ")
	pw, err := terminal.ReadPassword(int(syscall.Stdin))    
	if err != nil {
		fmt.Println("Unable to get password. Exiting")
		os.Exit(0)
	}

	if !confirm {
		return pw
	}
	fmt.Println()
	fmt.Print("Repeat Password: ")
	pw2, err := terminal.ReadPassword(int(syscall.Stdin))    
	if err != nil {
		fmt.Println("Unable to get password. Exiting")
		os.Exit(0)
	}

	if bytes.Equal(pw, pw2) {
		return pw
	}
	fmt.Println("Passwords do not match. Exiting")
	os.Exit(0)
	return nil
}

func firstTime() bool {
	userHome, _ := os.UserHomeDir()
	_, err := os.Stat(userHome + string(os.PathSeparator) + "my_pwds_ver")
    return os.IsNotExist(err)
}

func initialize() []byte {
	userHome, _ := os.UserHomeDir()
	userPw := readPassword(true)
	userKey, salt, _ := util.BuildKey(userPw, nil)
	ref := util.EncryptToString(userKey, "hello")
	ref_file, _ := os.Create(userHome + string(os.PathSeparator) + "my_pwds_ver")
	ref_file.WriteString(base64.StdEncoding.EncodeToString(salt) + "#" + ref)
	ref_file.Close()
	return userKey
}

func authenticate() []byte {
	userPw := readPassword(false)	
	userHome, _ := os.UserHomeDir()
	content, err := ioutil.ReadFile(userHome + string(os.PathSeparator) + "my_pwds_ver")
	
	if err != nil {
		fmt.Println("Unable to get password. Exiting")
		os.Exit(0)
	}
	str := string(content)
	parts := strings.SplitN(str, "#", 2)
	salt, _ := base64.StdEncoding.DecodeString(parts[0])
	userKey, _, _ := util.BuildKey(userPw, salt)
	pt := util.DecryptString(userKey, parts[1])

	if err != nil || pt != "hello" {
		fmt.Println("Incorrect password. Exiting")
		fmt.Println(err)
		os.Exit(0)
	}

	return userKey
}

func main() {
	/*
	The user can provide a database file name optionally.
	If not provided, os.Args will only be of length 1, containing
	the program name. If provided, we use the file name.
	*/
	db_arg := DefaultDbFile
	if len(os.Args) > 1 {
		// Make sure we discard spaces in input
		db_arg = strings.TrimSpace(os.Args[1])
		if db_arg == "" {
			db_arg = DefaultDbFile
		}
	}

	// We store the file in user's home directory
	userHome, _ := os.UserHomeDir()

	var userKey []byte
	if firstTime() {
		userKey = initialize()
	} else {
		userKey = authenticate()
	}

	db_file := userHome + string(os.PathSeparator) + db_arg

	// Initialize an empty database
	db := util.CSVDatabase {[]util.Account {}}

	// Create a buffered reader which optimizes reading the stream
	reader := bufio.NewReader(os.Stdin)
	fmt.Println()
	// Loop infinitely until a special command to exit the loop
	for {
		// Prompt the user for a command
		fmt.Print("> ")
		// Wait for the command
		command, err := readTrimmed(reader)

		// If error in reading exit
		if err != nil {
			fmt.Println(err)
			os.Exit(0)
		}

		// "l" for list of accounts
		// We use "switch" statement to handle multiple options
		switch command {
		case "l":
			// Load the database
			err := db.Read(db_file, userKey)

			// Error handling
			if err != nil {
				fmt.Println("Error reading database")
				continue
			}

			// Iterate the accounts to print the account name
			// in each line
			for _, account := range db.Accounts {
				fmt.Println(account.AccountName)
			}
		case "g":
			// Get the account name to fetch
			acc_name, _ := promptFor("Account Name", reader)
			// Trim spaces and handle empty input
			acc_name = strings.TrimSpace(acc_name)
			if acc_name == "" {
				fmt.Println("Not found")
				continue
			}
			err := db.Read(db_file, userKey)
			if err != nil {
				fmt.Println("Error reading database")
				continue
			}
			index := -1
			for i, account := range db.Accounts {
				if account.AccountName == acc_name {
					index = i
					break
				}
			}
			if index != -1 {
				acc := db.Accounts[index]
				fmt.Println(acc.UserName + ": " + acc.Password)
			} else {
				fmt.Println("Not found")
			}
		case "d": // handle deletion
			// Get the account name to delete
			acc_name, _ := promptFor("Account Name", reader)

			// Trim spaces and handle empty input
			acc_name = strings.TrimSpace(acc_name)
			if acc_name == "" {
				fmt.Println("Nothing to delete")
				continue
			}

			// Load DB
			err := db.Read(db_file, userKey)
			if err != nil {
				fmt.Println("Error reading database")
				continue
			}

			// Search for matching account name.
			// If found, note the index for removal later
			index := -1
			for i, account := range db.Accounts {
				if account.AccountName == acc_name {
					index = i
					break
				}
			}

			// If account found, we remove that element by appending
			// the array slices before the element and after the element
			// effectively removing the found element
			// Then, clear the current database and write
			// the records in memory to file
			if index != -1 {
				db.Accounts = append(db.Accounts[:index], db.Accounts[index+1:]...)
				os.Remove(db_file)
				for _, account := range db.Accounts {
					newAcc := util.Account { account.AccountName, 
						account.UserName, account.Password }
					db.Write(db_file, &newAcc, userKey)
				}
			} else {
				fmt.Println("Nothing to delete")
			}
		case "a": // to add a new account
			// Ask for account name, user name, password
			acc_name, err := promptFor("Account Name", reader)
			if err != nil || acc_name == "" {
				continue
			}
			user_name, err := promptFor("User Name", reader)
			if err != nil {
				continue
			}
			pw, err := promptFor("Password", reader)
			if err != nil {
				continue
			}
			// Write the new account to the database
			db.Write(db_file, &util.Account {acc_name, user_name, pw}, userKey)
		case "q": // to quit the program
			fmt.Println("Bye")
			os.Exit(0)
		default:
			// nothing
		}
	}
}
