package util

import (
	"os"
	"encoding/csv"
)

/* A user account */
type Account struct {
	AccountName string
	UserName string
	Password string
}

type Database struct {
	Accounts []Account
}

/* Interface to the database */
type DatabaseAccessor interface {

	/* Write a new account into database */
	Write(string, *Account) error

	/* Read all accounts from the database */
	Read(string) error
}

/* CSV Implementation of Database */
type CSVDatabase Database

/* Write to the CSV database */
func (db *CSVDatabase) Write(db_file string, account *Account) error {
	file, err := os.OpenFile(db_file, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0755)
	if err != nil {
		return err
	}

	writer := csv.NewWriter(file)
	data := []string { account.AccountName, account.UserName, account.Password }
	if err := writer.Write(data); err !=nil {
		file.Close()
		return err
	}

	writer.Flush()
	file.Close()
	if err := writer.Error(); err != nil {
		return err
	}

	return nil
}

/* Read from CSV database */
func (db *CSVDatabase) Read(db_file string) error {
	file, err := os.Open(db_file)
	if err != nil {
		return err
	}

	reader := csv.NewReader(file)
	data, err := reader.ReadAll()
	if err != nil {
		return err
	}
	
	db.Accounts = make([]Account, len(data))

	for i, d := range data {
		db.Accounts[i] = Account{d[0], d[1], d[2]}
	}

	file.Close()
	return nil
}

