// Create an executable
package main

// Packages we use in this application
import (
    "log"
	"net/http"
	"fmt"
	"io/ioutil"
	"time"
	"github.com/tidwall/gjson"
)

// The CoinGecko API to talk with to fetch the market prices
const COIN_EP = "https://api.coingecko.com/api/v3/simple/price?ids=%s&vs_currencies=USD"


func movAvg() func(float64) float64 {
	var avg float64
	count := 0.0
	return func(price float64) float64 {
		count = count + 1
		avg = avg + (price - avg) / count
		return avg
	}
}


func fetch(channel chan float64, avgFunc func(float64) float64, coinId string) {
	resp, err := http.Get(fmt.Sprintf(COIN_EP, coinId))

	if err != nil {
		log.Fatalln(err)
		channel <- 0.0
		return
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
		channel <- 0.0
		return
	}

	val := gjson.Get(string(body), "*.usd")
	resp.Body.Close()
	channel <- avgFunc(val.Float())
}

func fetchAll(btc_chan, ltc_chan, eth_chan chan float64) {
	btcAvg := movAvg()
	ltcAvg := movAvg()
	ethAvg := movAvg()

	i := 10
	for i > 0 {
		fetch(btc_chan, btcAvg, "bitcoin")
		fetch(ltc_chan, ltcAvg, "litecoin")
		fetch(eth_chan, ethAvg, "ethereum")
		i--
		time.Sleep(1* time.Minute)
	}
}

func printAll(btc_chan, ltc_chan, eth_chan chan float64) {
	i := 10
	for i > 0 {
		fmt.Printf("BTC:%f, LTC:%f, ETH:%f\n", <-btc_chan, <-ltc_chan, <-eth_chan)
		i--
	}
}

func main() {
	btcChan := make(chan float64)
	ltcChan := make(chan float64)
	ethChan := make(chan float64)

	fetchAll(btcChan, ltcChan, ethChan)

	printAll(btcChan, ltcChan, ethChan)	
}
