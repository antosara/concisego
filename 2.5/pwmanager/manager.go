package main

import (
	"os"
	"fmt"
	"bufio"	
	"pwmanager/util"
	"strings"
)

const DefaultDbFile = "my_pwds.csv"

func readTrimmed(reader *bufio.Reader) (string, error) {
	str, err := reader.ReadString('\n')
	return strings.TrimSpace(str), err
}

func promptFor(prompt string, reader *bufio.Reader) (val string, err error) {
	fmt.Print(" ", prompt, ": ")
	val, err = readTrimmed(reader)
	if err != nil {
		return "", nil
	}
	return
}

func main() {
	/*
	The user can provide a database file name optionally.
	If not provided, os.Args will only be of length 1, containing
	the program name. If provided, we use the file name.
	*/
	db_arg := DefaultDbFile
	if len(os.Args) > 1 {
		// Make sure we discard spaces in input
		db_arg = strings.TrimSpace(os.Args[1])
		if db_arg == "" {
			db_arg = DefaultDbFile
		}
	}

	// We store the file in user's home directory
	userHome, _ := os.UserHomeDir()
	db_file := userHome + string(os.PathSeparator) + db_arg

	// Initialize an empty database
	db := util.CSVDatabase {[]util.Account {}}

	// Create a buffered reader which optimizes reading the stream
	reader := bufio.NewReader(os.Stdin)

	// Loop infinitely until a special command to exit the loop
	for {
		// Prompt the user for a command
		fmt.Print("> ")
		// Wait for the command
		command, err := readTrimmed(reader)

		// If error in reading exit
		if err != nil {
			fmt.Println(err)
			os.Exit(0)
		}

		// "l" for list of accounts
		// We use "switch" statement to handle multiple options
		switch command {
		case "l":
			// Load the database
			err := db.Read(db_file)

			// Error handling
			if err != nil {
				fmt.Println("Error reading database")
				continue
			}

			// Iterate the accounts to print the account name
			// in each line
			for _, account := range db.Accounts {
				fmt.Println(account.AccountName)
			}
		case "g":
			// Get the account name to fetch
			acc_name, _ := promptFor("Account Name", reader)
			// Trim spaces and handle empty input
			acc_name = strings.TrimSpace(acc_name)
			if acc_name == "" {
				fmt.Println("Not found")
				continue
			}
			err := db.Read(db_file, userKey)
			if err != nil {
				fmt.Println("Error reading database")
				continue
			}
			index := -1
			for i, account := range db.Accounts {
				if account.AccountName == acc_name {
					index = i
					break
				}
			}
			if index != -1 {
				acc := db.Accounts[index]
				fmt.Println(acc.UserName + ": " + acc.Password)
			} else {
				fmt.Println("Not found")
			}				
		case "d": // handle deletion
			// Get the account name to delete
			acc_name, _ := promptFor("Account Name", reader)

			// Trim spaces and handle empty input
			acc_name = strings.TrimSpace(acc_name)
			if acc_name == "" {
				fmt.Println("Nothing to delete")
				continue
			}

			// Load DB
			err := db.Read(db_file)
			if err != nil {
				fmt.Println("Error reading database")
				continue
			}

			// Search for matching account name.
			// If found, note the index for removal later
			index := -1
			for i, account := range db.Accounts {
				if account.AccountName == acc_name {
					index = i
					break
				}
			}

			// If acount found, we remove that element by appending
			// the array slices before the element and after the element
			// effectively removing the found element
			// Then, clear the current database and write
			// the records in memory to file
			if index != -1 {
				db.Accounts = append(db.Accounts[:index], db.Accounts[index+1:]...)
				os.Remove(db_file)
				for _, account := range db.Accounts {
					newAcc := util.Account { account.AccountName, 
						account.UserName, account.Password }
					db.Write(db_file, &newAcc )
				}
			} else {
				fmt.Println("Nothing to delete")
			}
		case "a": // to add a new account
			// Ask for account name, user name, password
			acc_name, err := promptFor("Account Name", reader)
			if err != nil || acc_name == "" {
				continue
			}
			user_name, err := promptFor("User Name", reader)
			if err != nil {
				continue
			}
			pw, err := promptFor("Password", reader)
			if err != nil {
				continue
			}
			// Write the new account to the database
			db.Write(db_file, &util.Account {acc_name, user_name, pw} )
		case "q": // to quit the program
			fmt.Println("Bye")
			os.Exit(0)
		default:
			// nothing
		}
	}
}
