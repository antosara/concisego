package util

import (	
	"testing"
)

func TestBuildKey(t *testing.T) {
	key, salt, _ := BuildKey([]byte("my_pw"), nil)
	if key == nil || salt == nil {
		t.Errorf("Expected write did not happen")
	}
}

func TestEncDec(t *testing.T) {
	key, _, _ := BuildKey([]byte("my_pw"), nil)
	ciphertext, nonce, err := Encrypt([]byte("secret"), key)

	if err != nil {
		t.Errorf("Encryption failed")
	}
 	plaintext, err := Decrypt(ciphertext, key, nonce)

	if err != nil {
		t.Errorf("Decryption failed")
	}

	if string(plaintext) != "secret" {
		t.Errorf("Could not retrieve secret")
	}
}