package util

import (
	"os"
	"testing"
)

func TestWrite(t *testing.T) {
	os.Remove("test_db.csv")
	newAcc := Account {"acc","user","pw"}
	db := CSVDatabase {[]Account {}}
	key, _, _ := BuildKey([]byte("my_pw"), nil)
	err := db.Write("test_db.csv", &newAcc, key)
	if err != nil {
		t.Errorf("Expected write did not happen")
		t.Log(err)
	}

	err = db.Read("test_db.csv", key)

	if err != nil {
		t.Errorf("Expected read did not happen")
		t.Log(err)
	}

	acc_len := len(db.Accounts)
	if acc_len != 1 {
		t.Errorf("Expected accounts length: 1, Got %d", acc_len)
	}
	if db.Accounts[0].AccountName != "acc" {
		t.Errorf("Expected account name to be 'acc'")
	}
	os.Remove("test_db.csv")
}

func TestRead(t *testing.T) {
	var dbAccessor DatabaseAccessor = &CSVDatabase {[]Account {}}
	key, _, _ := BuildKey([]byte("my_pw"), nil)
	err := dbAccessor.Read("test_db1.csv", key)

	if(err == nil) {
		t.Errorf("Expected error not thrown")
	}	
}
